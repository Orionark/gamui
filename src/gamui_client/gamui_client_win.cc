#include "stdio.h"

#include "gamui_engine.h"

int main(int argc, char* argv[])
{  
  gamui::GamuiEngine* gamuiEngine = gamui::GamuiEngine::CreateEngine();
  gamui::GamuiViewClient* gamuiViewClient = gamuiEngine->CreateView();

  gamuiViewClient->LoadXML("<test>Hello World!</test>");

  for(int i = 0; i < 100000; i++)
  {
    gamuiViewClient->Process();
  }

  gamuiEngine->Shutdown();
}