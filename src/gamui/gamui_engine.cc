#include "stdio.h"

#include "gamui_engine.h"
#include "common/message_queue.h"
#include "ui/ui_manager.h"

using namespace gamui;

class PrivateGamuiEngine : public GamuiEngine
{
public:
  PrivateGamuiEngine()
  {
    uiManager.Initialize();
  }

  ~PrivateGamuiEngine() override
  {
    uiManager.Shutdown();
  }

  GamuiViewClient* CreateView() override
  {
    return uiManager.CreateView();
  }

private:
  UiManager uiManager;
};

GamuiEngine* GamuiEngine::CreateEngine()
{
  return new PrivateGamuiEngine();
}

GamuiEngine::GamuiEngine()
{

}

GamuiEngine::~GamuiEngine()
{

}

void GamuiEngine::Shutdown()
{
  delete this;
}