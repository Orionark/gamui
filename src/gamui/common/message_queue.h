#pragma once

#include <atomic>
#include <stdio.h>

#include "common/types.h"

struct GamuiMessageDefinition
{
  uint32 type;
  uint32 size;
};

template <uint32 Size, typename T = void>
struct GamuiMessageData
{
  uint32 type = 0;
  T* data = nullptr;
  uint64 dataSize = 0;
  int8 buffer[Size] = {0};
};

template <uint32 Count, uint32 Size>
struct GamuiMessageQueueBlock
{
  uint32 id = 0;
  GamuiMessageQueueBlock<Count,Size>* next = nullptr;
  std::atomic<int32> head = 0;
  std::atomic<int32> tail = -1;
  GamuiMessageData<Size> data[Count];

  GamuiMessageQueueBlock(uint32 id)
    : id(id)
  {

  }

  GamuiMessageData<Size>* Add(void* filled = nullptr)
  {
    int32 loadedHead = head.load();
    int32 loadedTail = tail.load();
    //printf("[%d] Add: %p %d/%d\n", id, filled, loadedHead, loadedTail);

    int32 useTail = loadedTail;
    if(useTail == Count-1)
    {
      useTail = -1;
    }

    if(loadedTail != -1 && loadedHead != Count)
    {
      int32 diff = loadedTail - loadedHead;
      if(diff == Count-1)
      {
        return nullptr;
      }
      if(diff == -1)
      {
        GamuiMessageData<Size>& next = data[useTail + 1];
        if(filled && filled == &next)
        {
          tail = useTail + 1;
          //printf("[%d] Tail: %d\n", id, (useTail+1));
        }
        else if(next.type != 0)
        {
          return nullptr;
        }

        return &next;
      }
    }

    GamuiMessageData<Size>& next = data[useTail + 1];
    if(filled && filled == &next)
    {
      tail = useTail + 1;
      //printf("[%d] Tail: %d\n", id, (useTail+1));
    }
    else if(next.type != 0)
    {
      //printf("Add Error %d [%d] [%d]\n", next.type, useTail, (Size-1));
      return nullptr;
    }

    return &next;
  }

  GamuiMessageData<Size>* Shift(void* released)
  {
    int32 loadedHead = head.load();
    int32 loadedTail = tail.load();
    //printf("[%d] Shift: %p %d/%d\n", id, released, loadedHead, loadedTail);

    if(loadedHead == Count)
    {
      loadedHead = 0;
    }
    GamuiMessageData<Size>& next = data[loadedHead];
    if(next.type == 0)
    {
      if(released)
      {
        //printf("Shift Error\n");
      }
      return nullptr;
    }
    if(released == &next)
    {
      next.type = 0;
      head = loadedHead + 1;
      //printf("[%d] Head: %d\n", id, (loadedHead+1));
      if(next.data != &next.buffer[0])
      {
        delete next.data;
      }
    }

    return &next;
  }
};

template <uint32 Count, uint32 Size>
class GamuiMessageQueue
{
public:
  GamuiMessageQueue()
  {
    auto block = new GamuiMessageQueueBlock<Count,Size>(counter++);
    block->next = block;
    head = block;
    tail = block;
  }

  ~GamuiMessageQueue()
  {
    GamuiMessageQueueBlock<Count,Size>* block = head.load();
    GamuiMessageQueueBlock<Count,Size>* start = block;
    while(block)
    {
      GamuiMessageQueueBlock<Count,Size>* old = block;
      block = block->next;
      if(block == start)
      {
        block = nullptr;
      }
      delete old;
    }
    head = nullptr;
    tail = nullptr;
  }

  template <typename T>
  GamuiMessageData<Size, T>* Create()
  {
    GamuiMessageData<Size, T>* next = Add<T>();
    next->data = reinterpret_cast<T*>(&next->buffer[0]);
    uint64 dataSize = sizeof(T);
    if(dataSize > Size)
    {
      next->data = reinterpret_cast<T*>(new int8[dataSize]);
    }
    next->dataSize = dataSize;
    new (next->data) T();
    return reinterpret_cast<GamuiMessageData<Size, T>*>(next);
  }

  template <class T>
  GamuiMessageData<Size, T>* Add(GamuiMessageData<Size, T>* filled = nullptr)
  {
    if(T::type == 0)
    {
      return nullptr;
    }

    if(filled)
    {
      filled->type = T::type;
    }

    GamuiMessageQueueBlock<Count,Size>* loadedTail = tail.load();
    GamuiMessageData<Size>* next = loadedTail->Add(filled);
    if(!next)
    {
      GamuiMessageQueueBlock<Count,Size>* prev = loadedTail->next;
      if(prev != head.load())
      {
        next = prev->Add();
      }
      if(!next)
      {
        loadedTail->next = new GamuiMessageQueueBlock<Count,Size>(counter++);
        loadedTail->next->next = prev;
        next = loadedTail->next->Add();
      }
      //printf("Tail is now %d\n", loadedTail->next->id);
      tail = loadedTail->next;
    }

    return reinterpret_cast<GamuiMessageData<Size, T>*>(next);
  }

  uint32 PeekType()
  {
    GamuiMessageData<Size>* next = Shift<void>(nullptr);
    if(next)
    {
      return next->type;
    }
    return 0;
  }

  template <typename T = void>
  GamuiMessageData<Size, T>* Peek()
  {
    GamuiMessageData<Size>* next = Shift<void>(nullptr);
    return reinterpret_cast<GamuiMessageData<Size,T>*>(next);
  }

  template <typename T>
  GamuiMessageData<Size>* Shift(GamuiMessageData<Size, T>* released)
  {
    GamuiMessageQueueBlock<Count,Size>* loadedTail = tail.load();
    GamuiMessageQueueBlock<Count,Size>* loadedHead = head.load();
    GamuiMessageData<Size>* next = loadedHead->Shift(released);
    if(!next && loadedHead != loadedTail)
    {
      loadedHead = loadedHead->next;
      next = loadedHead->Shift(released);
      if(next)
      {
        //printf("Head is now %d\n", loadedHead->id);
        head = loadedHead;
      }
    }
    if(released && (uintptr)released == (uintptr)next)
    {
      released->data->~T();
    }
    return next;
  }

private:
  uint32 counter = 1;

  std::atomic<GamuiMessageQueueBlock<Count,Size>*> head = nullptr;
  std::atomic<GamuiMessageQueueBlock<Count,Size>*> tail = nullptr;
};