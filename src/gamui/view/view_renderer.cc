#include "stdio.h"

#include "view/view_renderer.h"
#include "display/viewport.h"
#include "view/view_messages.h"

using namespace gamui;

ViewRenderer::ViewRenderer(Viewport* viewport)
  : viewport(viewport)
{

}

ViewRenderer::~ViewRenderer()
{

}

void ViewRenderer::Process()
{
  bool bContinue = true;
  while(bContinue)
  {
    uint32 type = out.PeekType();

    if(type == 0)
    {
      break;
    }

    switch(type)
    {
      default:
        bContinue = false;
    }
  }
}

void ViewRenderer::Initialize()
{
  bInitialized = true;
}