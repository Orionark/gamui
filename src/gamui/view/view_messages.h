#pragma once

#include <stdio.h>
#include <string>

#include "common/types.h"

namespace gamui
{
  struct ViewMessage_LoadXML
  {
    static const uint32 type = 1;

    std::string xml;
  };
}