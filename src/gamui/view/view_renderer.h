#pragma once

#include "common/message_queue.h"

namespace gamui
{
  typedef GamuiMessageQueue<10,512> RendererMessageQueue;

  class Viewport;

  class ViewRenderer
  {
  public:
    ViewRenderer(Viewport* viewport);
    ~ViewRenderer();

    void Process();
    void Initialize();

    bool IsInitialized()
    {
      return bInitialized;
    }

    RendererMessageQueue& GetIncomingMessageQueue()
    {
      return in;
    }

    RendererMessageQueue& GetOutgoingMessageQueue()
    {
      return out;
    }

    

  private:
    bool bInitialized = false;
    RendererMessageQueue in;
    RendererMessageQueue out;

    Viewport* viewport;
  };
}