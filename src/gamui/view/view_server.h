#pragma once

#include "memory.h"

#include "display/viewport.h"
#include "view/view_renderer.h"
#include "common/message_queue.h"

namespace tinyxml2
{
  class XMLNode;
}

namespace gamui
{
  class GpuManager;

  typedef GamuiMessageQueue<10,512> ServerMessageQueue;

  class ViewServer
  {
  public:
    ViewServer(GpuManager* gpuManager);
    ~ViewServer();

    //Methods called on the UI thread
    bool IsInitialized()
    {
      return bInitialized;
    }
    void Initialize();
    void Process();

    //Messages destined for the main thread
    ServerMessageQueue& GetIncomingMessageQueue()
    {
      return in;
    }

    //Messages destined for the UI thread
    ServerMessageQueue& GetOutgoingMessageQueue()
    {
      return out;
    }

  private:
    void LoadXML(const char* xml, uint32 size);
    void ParseXMLNode(const tinyxml2::XMLNode* node, Container* parent);

    bool bInitialized = false;

    GpuManager* gpuManager;

    Viewport viewport;
    ViewRenderer viewRenderer;

    ServerMessageQueue in;
    ServerMessageQueue out;
  };
}