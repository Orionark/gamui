#include "stdio.h"

#include "view/view_server.h"
#include "gpu/gpu_manager.h"
#include "view/view_messages.h"

#include "tinyxml2.h"

using namespace gamui;
using namespace tinyxml2;

ViewServer::ViewServer(GpuManager* gpuManager)
  : gpuManager(gpuManager)
  , viewRenderer(&viewport)
{

}

ViewServer::~ViewServer()
{
  
}

void ViewServer::Initialize()
{
  bInitialized = true;
  gpuManager->AddViewRenderer(&viewRenderer);
}

void ViewServer::Process()
{
  bool bContinue = false;
  while(true)
  {
    uint32 type = out.PeekType();

    if(type == 0)
    {
      break;
    }

    switch(type)
    {
      case ViewMessage_LoadXML::type:
        {
          auto msg = out.Peek<ViewMessage_LoadXML>();
          LoadXML(msg->data->xml.c_str(), msg->data->xml.size());
          out.Shift(msg);
        }
        break;

      default:
        bContinue = false;
    }
  }
}

void ViewServer::LoadXML(const char* xml, uint32 size)
{
  XMLDocument doc;
  doc.Parse(xml, size);
  if(doc.Error())
  {
    printf("Error parsing XML");
    return;
  }
  ParseXMLNode(&doc, viewport.GetRootContainer());
}

void ViewServer::ParseXMLNode(const XMLNode* node, Container* parent)
{
  parent->Clear();
  const XMLNode* child = node->FirstChild();
  while(child)
  {
    const XMLElement* element = child->ToElement();
    if(element)
    {
      ContainerRef container = parent->CreateContainer();
      parent->AddChild(container);
      printf("<%s>\n", element->Name());
      ParseXMLNode(child, container);
    }

    const XMLText* text = child->ToText();
    if(text)
    {
      parent->SetText(text->Value());
      printf("Text: %s\n", text->Value());
    }

    child = child->NextSibling();
  }
}