#pragma once

#include "gamui_view_client.h"
#include "view/view_server.h"

namespace gamui
{
  class UiManager;
  class GpuManager;
  class ViewClientImpl;

  class ViewClientImpl : public GamuiViewClient
  {
  public:
    ViewClientImpl(GpuManager* gpuManager);
    ~ViewClientImpl() override;

    void Process() override;
    void LoadXML(const char* xml) override;

    ViewServer viewServer;
  };

  struct ViewList
  {
    ViewList* previous;
    ViewClientImpl* viewClient;
  };
}