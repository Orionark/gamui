#include "stdio.h"

#include "view/view_client_impl.h"
#include "view/view_messages.h"

using namespace gamui;

GamuiViewClient::GamuiViewClient()
{

}

GamuiViewClient::~GamuiViewClient()
{

}

ViewClientImpl::ViewClientImpl(GpuManager* gpuManager)
  : viewServer(gpuManager)
{
  
}

ViewClientImpl::~ViewClientImpl()
{
  
}

void ViewClientImpl::Process()
{
  
}

void ViewClientImpl::LoadXML(const char* xml)
{
  auto& mq = viewServer.GetOutgoingMessageQueue();
  auto msg = mq.Create<ViewMessage_LoadXML>();
  msg->data->xml = xml;
  mq.Add(msg);
}