#pragma once

#include <string>

#include "common/types.h"

namespace gamui
{
  class Container;

  class ContainerRef
  {
  public:
    ContainerRef(Container* container);
    ContainerRef(ContainerRef& other);
    ~ContainerRef();

    Container* operator->();
    ContainerRef& operator=(ContainerRef& other);

    operator Container*() { return container; };

  private:
    Container* container;
  };

  class Container
  {
  public:
    Container();
    Container(Container* owner);
    ~Container();

    void Clear();
    ContainerRef CreateContainer();
    void DestroyContainer(Container* container);
    bool AddChild(Container* child);
    bool RemoveChild(Container* child);

    void Claim();
    void Release();

    void SetText(const char* text)
    {
      this->text = text;
      bModified = true;
    }
    const char* GetText()
    {
      return text.c_str();
    }

    bool IsDirty();

  private:
    uint32 references = 0;

    std::string text;
    bool bModified = false;

    Container* owner = nullptr;
    Container* next = nullptr;
    Container* previous = nullptr;
    Container* first = nullptr;
    Container* last = nullptr;
    Container* parent = nullptr;

    friend class ContainerRef;
  };
}