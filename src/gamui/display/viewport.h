#pragma once

#include "display/container.h"

namespace gamui
{
  class Viewport
  {
  public:
    Viewport();
    ~Viewport();

    void Resize(int width, int height);

    Container* GetRootContainer()
    {
      return &root;
    }

  private:
    Container root;
  };
}