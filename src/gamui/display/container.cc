#include "display/container.h"

using namespace gamui;

ContainerRef::ContainerRef(Container* container)
  : container(container)
{
  container->Claim();
}

ContainerRef::~ContainerRef()
{
  container->Release();
}

Container* ContainerRef::operator->()
{
  return container;
}

ContainerRef& ContainerRef::operator=(ContainerRef& other)
{
  Container* oldContainer = container;
  container = other.container;
  container->Claim();
  if(oldContainer)
  {
    oldContainer->Release();
  }
  return *this;
}

Container::Container()
{

}

Container::Container(Container* owner)
  : owner(owner)
{

}

Container::~Container()
{
  
}

void Container::Claim()
{
  references++;
}

void Container::Release()
{
  references--;
  if(references == 0 && owner)
  {
    owner->DestroyContainer(this);
  }
}

void Container::Clear()
{
  Container* next = first;
  while(next)
  {
    Container* del = next;
    next = next->next;
    RemoveChild(del);
  }
}

ContainerRef Container::CreateContainer()
{
  return ContainerRef(new Container(owner ? owner : this));
}

void Container::DestroyContainer(Container* container)
{
  if(container->owner == this && container->references == 0)
  {
    container->owner = nullptr;
    delete container;
  }
}

bool Container::AddChild(Container* container)
{
  if(container->parent == this)
  {
    return false;
  }

  bModified = true;

  container->Claim();

  if(container->parent)
  {
    container->parent->RemoveChild(container);
  }

  if(!last)
  {
    first = last = container;
    return true;
  }

  last->next = container;
  container->previous = last;
  last = container;

  return true;
}

bool Container::RemoveChild(Container* container)
{
  if(container->parent != this)
  {
    return false;
  }

  bModified = true;

  if(container == first)
  {
    first = container->next;
  }

  if(container == last)
  {
    last = container->previous;
  }

  if(container->previous)
  {
    container->previous->next = container->next;
  }

  if(container->next)
  {
    container->next->previous = container->previous;
  }

  container->parent = nullptr;

  container->Release();

  return true;
}

bool Container::IsDirty()
{
  if(bModified)
  {
    bModified = false;
    return true;
  }
  return false;
}