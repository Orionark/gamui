#pragma once

#include <thread>
#include <atomic>

#include "common/message_queue.h"
#include "gpu/gpu_manager.h"

namespace gamui
{
  struct ViewList;

  class UiManager
  {
  public:
    UiManager();
    ~UiManager();

    void Initialize();
    void Shutdown();

    class GamuiViewClient* CreateView();

  private:
    void Process();
    
    std::thread uiThread;

    bool bInitialized = false;
    bool bShuttingDown = false;
    std::atomic<ViewList*> tail = nullptr;

    GpuManager gpuManager;
  };
}