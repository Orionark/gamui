#include "stdio.h"

#include "ui/ui_manager.h"
#include "view/view_client_impl.h"
#include "view/view_server.h"

using namespace gamui;

UiManager::UiManager()
{

}

UiManager::~UiManager()
{

}

GamuiViewClient* UiManager::CreateView()
{
  ViewList* viewList = new ViewList();
  viewList->previous = tail;
  viewList->viewClient = new ViewClientImpl(&gpuManager);
  tail = viewList;
  return viewList->viewClient;
}

void UiManager::Process()
{
  //printf("UiManager::Process()\n");

  ViewList* next = tail;
  while(next)
  {
    ViewClientImpl* client = next->viewClient;
    next = next->previous;
    ViewServer* server = &client->viewServer;
    if(!server->IsInitialized())
    {
      server->Initialize();
    }
    server->Process();
  }
}

void UiManager::Initialize()
{
  if(bInitialized)
  {
    return;
  }

  bInitialized = true;
  gpuManager.Initialize();

  UiManager* uiManager = this;
  std::thread thr([uiManager]{
    while(!uiManager->bShuttingDown)
    {
      uiManager->Process();
    }
  });
  uiThread.swap(thr);
}

void UiManager::Shutdown()
{
  bShuttingDown = true;

  //printf("UiManager shutting down GpuManager\n");
  gpuManager.Shutdown();
  //printf("UiManager GpuManager shut down\n");

  uiThread.join();
  //printf("UiManager thread joined\n");

  ViewList* next = tail;
  while(next)
  {
    ViewList* cur = next;
    next = cur->previous;
    delete cur->viewClient;
    delete cur;
  }
}