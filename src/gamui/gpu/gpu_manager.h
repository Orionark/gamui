#pragma once

#include <thread>
#include <atomic>

#include "common/message_queue.h"

namespace gamui
{
  class ViewRenderer;
  struct RendererList;

  typedef GamuiMessageQueue<10,512> GpuMessageQueue;
  
  class GpuManager
  {
  public:
    GpuManager();
    ~GpuManager();

    void Initialize();
    void Shutdown();

    void AddViewRenderer(ViewRenderer* viewRenderer);

  private:
    void Process();

    std::atomic<RendererList*> tail = nullptr;

    std::thread gpuThread;

    bool bInitialized = false;
    bool bShuttingDown = false;
  };
}