#include "stdio.h"

#include "gpu/gpu_manager.h"
#include "view/view_renderer.h"

namespace gamui
{
  struct RendererList
  {
    RendererList* previous;
    ViewRenderer* viewRenderer;
  };
}

using namespace gamui;

GpuManager::GpuManager()
{

}

GpuManager::~GpuManager()
{

}

void GpuManager::Process()
{
  //printf("GpuManager::Process()\n");
  RendererList* next = tail.load();
  while(next)
  {
    ViewRenderer* viewRenderer = next->viewRenderer;
    if(!viewRenderer->IsInitialized())
    {
      viewRenderer->Initialize();
    }
    viewRenderer->Process();
    next = next->previous;
  }
}

void GpuManager::AddViewRenderer(ViewRenderer* viewRenderer)
{
  //printf("GpuManager::AddViewRenderer()\n");
  RendererList* next = new RendererList();
  next->viewRenderer = viewRenderer;
  next->previous = tail.load();
  tail = next;
}

void GpuManager::Initialize()
{
  if(bInitialized)
  {
    return;
  }

  bInitialized = true;
  
  GpuManager* gpuManager = this;
  std::thread thr([gpuManager]{
    while(!gpuManager->bShuttingDown)
    {
      gpuManager->Process();
    }
  });
  gpuThread.swap(thr);
}

void GpuManager::Shutdown()
{
  //printf("GpuManager shutting down\n");
  bShuttingDown = true;
  gpuThread.join();
  //printf("GpuManager thread joined\n");
}