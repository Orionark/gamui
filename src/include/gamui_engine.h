#pragma once

#include "gamui_view_client.h"

#define LIBGAMUI_API _declspec(dllexport)

namespace gamui
{
  class LIBGAMUI_API GamuiEngine
  {
  public:
    virtual ~GamuiEngine();

    virtual GamuiViewClient* CreateView() = 0;

    static GamuiEngine* CreateEngine();
    virtual void Shutdown();

  protected:
    GamuiEngine();
  };
}