#pragma once

namespace gamui
{
  class GamuiViewClient
  {
  public:
    virtual ~GamuiViewClient();

    virtual void Process() = 0;
    virtual void LoadXML(const char* xml) = 0;

  protected:
    GamuiViewClient();
  };
}